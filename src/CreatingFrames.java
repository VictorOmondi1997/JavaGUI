import javax.swing.*;

public class CreatingFrames {
    public static void main(String[] args){
        JFrame frame = new JFrame("Test Frame");
        frame.setSize(400, 300);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add(new JButton("OK"));
    }
}
