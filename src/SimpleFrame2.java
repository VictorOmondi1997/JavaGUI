/*
* A program that sets several properties of the JFrame:
*
* @Author: Victor Omondi
*
* */

import javax.swing.*;
import java.awt.*;

public class SimpleFrame2 {
    public static void main(String[] args){
        JFrame frame = new JFrame();
        frame.setForeground(Color.blue);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocation(new Point(100, 50));
        frame.setSize(new Dimension(300,120));
        frame.setTitle("Victor Omondi");
        frame.setVisible(true);
    }
}
