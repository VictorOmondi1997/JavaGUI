/*
* @author Victor Omondi



 ************************
 * Package Importation   *
 * **********************/
import java.awt.*;
import javax.swing.*;
import java.awt.event.*;

public class MenuDemo {
    private JMenuItem mnuItemNew,
            mnuItemSave,
            mnuItemExit,
            mnuItemCopy,
            mnuItemPaste;

    /************************
     * Default Constructor  *
     * **********************/
    public MenuDemo(){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,300);
        frame.setVisible(true);
        frame.setLayout(new FlowLayout());

        JMenuBar mnuBar = new JMenuBar();
        frame.setJMenuBar(mnuBar);
        frame.setTitle("Menu Demo");

        JMenu mnuFile = new JMenu("File");
        JMenu mnuEdit = new JMenu("Edit");
        JMenu mnuHelp = new JMenu("Help");

        mnuItemNew = new JMenuItem("New");
        mnuItemSave = new JMenuItem("Save");
        mnuItemExit = new JMenuItem("Exit");
        mnuItemCopy = new JMenuItem("Copy");
        mnuItemPaste = new JMenuItem("Paste");

        //Add JMenu To The MenuBar
        mnuBar.add(mnuFile);
        mnuBar.add(mnuEdit);
        mnuBar.add(mnuHelp);

        //File JMenuItem
        mnuFile.add(mnuItemNew);
        mnuFile.add(new JSeparator());
        mnuFile.add(mnuItemSave);
        mnuFile.add(mnuItemExit);

        //Edit JMenuItem
        mnuEdit.add(mnuItemCopy);
        mnuEdit.add(mnuItemPaste);



        //Adding Action Listener
        mnuItemSave.addActionListener(new Handler());
        mnuItemCopy.addActionListener(new Handler());

        JMenu mnuTest = new JMenu("Test");
        mnuHelp.add(mnuTest);
        mnuTest.add(mnuItemExit);

    }

    /************************
     *     Main Method      *
     * **********************/
    public static void main(String[] s){
        new MenuDemo();
    }
    private class Handler implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e){
            if(e.getSource()==mnuItemSave)
                JOptionPane.showMessageDialog(null, "Document Saved");
            else
                JOptionPane.showMessageDialog(null, "Nothing to copy");
        }
    }
}
