import javax.swing.*;

public class ConfirmDialogExample {
    public static void main(String[] args){
        int choice = JOptionPane.showConfirmDialog(null, "Erase Your Hard Disk?");
        if(choice == JOptionPane.YES_OPTION){
            JOptionPane.showMessageDialog(null, "Disk Erased!");
        }else {
            JOptionPane.showMessageDialog(null, "Cancelled.");
        }
    }
}
