/*
* The following program attempts to who two buttons
*
* @Author: Victor Omondi
* */

import javax.swing.*;
import java.awt.*;

public class ComponentsExample1 {
    public static void main(String[] args){
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(new Dimension(300, 100));
        frame.setTitle("A Frame With Button: ");

        JButton button = new JButton();
        button.setText("I'm a button.");
        button.setBackground(Color.blue);
        frame.add(button);

        JButton button1 = new JButton();
        button1.setText("Click me!");
        button1.setBackground(Color.red);
        frame.add(button1);

        frame.setLayout(new FlowLayout());
        frame.setVisible(true);
    }
}
