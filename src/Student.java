import javax.swing.JOptionPane;

public class Student {
    private String regNo, name;
    private double feeBalance;

    Student(){
        name = JOptionPane.showInputDialog("Enter Name: ");
        regNo = JOptionPane.showInputDialog("Enter Registration Number for: "+name);
        feeBalance = Double.parseDouble(JOptionPane.showInputDialog("Enter Fee Payable: "));
    }
    public void setName(){
        name = JOptionPane.showInputDialog("Enter Your Name: ");
    }
    public void setRegNo(){
        regNo = JOptionPane.showInputDialog("Enter Registration Number: ");
    }
    public void getName(){
        JOptionPane.showMessageDialog(null, "Your Name is "+name);
    }
    public void getRegNo(){
        JOptionPane.showMessageDialog(null, name+" Your Registration number is: "+regNo);
    }
    public void getFeeBalance(){
        JOptionPane.showMessageDialog(null, name+" Your Current Fee is "+feeBalance);
        if(feeBalance<0){
            JOptionPane.showMessageDialog(null, name+" You Have OverPaid The Fees.");
        }
    }
    public void payFees(){
        double depositFees = Double.parseDouble(JOptionPane.showInputDialog(name+"How much Do You want to pay?"));
        feeBalance = feeBalance - depositFees;
    }
    public void getDetails(){
        int x = JOptionPane.showConfirmDialog(null, "Do You Want To Display "+name+"'s Details?");
        if(x==0) {
            JOptionPane.showMessageDialog(null, "The Following are your Details: \nName: " + name + "\nReg Number: " + regNo + " \nCurrent Fee Balance: " + feeBalance);
        }
    }
    public static void main(String[] args){
        Student vick = new Student();
        vick.getDetails();
        vick.setName();
        vick.getName();
        vick.setRegNo();
        vick.getRegNo();
        vick.payFees();
        vick.getFeeBalance();
        vick.getDetails();
    }
}
