import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FastGUI {
    private JLabel lblInput1;
    private JTextField txtInput1;
    private JLabel lblInput2;
    private JTextField txtInput2;
    private JLabel lblResult;
    private JTextField txtResult;
    private JButton sumButton;
    private JButton productButton;

    public FastGUI(){
        sumButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int sum = val1 + val2;
                txtResult.setText(" "+sum);
            }
        });
    }
    int val1 = Integer.parseInt(txtInput1.getText());
    int val2 = Integer.parseInt(txtInput2.getText());
    public static void main(String[] s){
        new FastGUI();
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
