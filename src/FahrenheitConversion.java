/*
* Write a temperature conversion program that converts from Fahrenheit to Celsius.
* The Fahrenheit temperature should be entered from the keyboard vial a dialog box.
* Another dialog box should be used to display the converted temperature.
* Use the formula for conversion: Celsius = 5/9*(Fahrenheit - 32).
*
* @Author : Victor Omondi.
* */

import javax.swing.JOptionPane;

public class FahrenheitConversion {
    public static void main(String[] args){
        double fahrenheit = Double.parseDouble(JOptionPane.showInputDialog("Enter Temperature in Fahrenheit."));
        double celsius = ((5.0/9)*(fahrenheit-32));
        JOptionPane.showMessageDialog(null, "Temperature " + fahrenheit + " Converted To Celsius = " + celsius);
    }
}
