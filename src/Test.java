import javax.swing.*;
import java.awt.*;


public class Test { //Class where we display our rectangle
    JFrame f;
    MyPanel p;
    public Test(){
        f = new JFrame();
        //get the content area of panel.
        Container c = f.getContentPane();

        //setLayout Manager
        c.setLayout(new BorderLayout());
        p = new MyPanel();
        //add MyPanel object int container
        c.add(p);
        //set the size of the JFrame
        f.setSize(400, 400);
        //make the JFrame Visible
        f.setVisible(true);
        //sets close behaviour; EXT_ON_CLOSE invokes System.exit(0) on closing the JFrame
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    public static void main(String args[]){
        Test t = new Test();
    }
}
