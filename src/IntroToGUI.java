import java.awt.FlowLayout;
import javax.swing.*;

public class IntroToGUI extends JFrame {
    private JLabel userName = new JLabel("User Name: ");
    private JTextField enterUserName = new JTextField(15);
    private JButton btnStartGame = new JButton("Start Game");

    public IntroToGUI(){
        super("Intro To GUI");
        setVisible(true);
        setSize(300,200);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new FlowLayout());
        add(userName);
        add(enterUserName);
        add(btnStartGame);
    }
    public static void main(String[] s){
        new IntroToGUI();
    }
}
