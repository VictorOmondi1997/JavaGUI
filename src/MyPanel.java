import javax.swing.*;
import java.awt.*;

public class MyPanel extends JPanel{
    //MyPanel extends Jpanel, which will eventually be placed in a JFrame

    //custom painting is performed by the painComponent method
    @Override
    public void paintComponent(Graphics g) {
        //clear the previous painting
        super.paintComponent(g);
        //cast Graphics2D
        Graphics2D g2 = (Graphics2D) g;
        g2.setColor(Color.red);  // sets graphics2D color
        //draw the rectangle
        g2.drawRect(0,0,100,100); //drewRect(x-position, y-position, width, height);
        g2.setColor(Color.blue);
        g2.fillRect(200, 0, 100, 100);
    }
}
