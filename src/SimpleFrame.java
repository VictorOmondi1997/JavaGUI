/*
* A simple program that creates and shows a JFrame:
*
* @Author: Victor Omondi
* */

//Importation
import javax.swing.*;

public class SimpleFrame {
    public static void main(String[] args){
        JFrame frame = new JFrame();
        frame.setVisible(true);
    }
}
