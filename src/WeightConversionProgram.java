/*
* A weight conversion program is required to convert units of weight from pounds into kilogrammes.
* The weight given in pounds is entered from the keyboard via a dialog box.
* Another dialog box should be used to display the converted weight in kilogrammes.
* (Use the following formula for conversion kilogramme = 2.2* pounds
* */


//Importation
import javax.swing.JOptionPane;

public class WeightConversionProgram {
    public static void main(String[] args){
        double weightInPounds = Double.parseDouble(JOptionPane.showInputDialog("Enter Weight in Pounds: "));
        double weightInKilograms = 2.2*weightInPounds;
        JOptionPane.showMessageDialog(null, "Weight in Pounds: "+weightInPounds+" Converted to Kilograms = "+weightInKilograms+"Kg");
    }
}
